#include "lzoIPBuildTime.h"

lzoIPBuildTime::lzoIPBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

lzoIPBuildTime::~lzoIPBuildTime()
{
}

const char* lzoIPBuildTime::GetDateTime()
{
    return dateTime;
}

