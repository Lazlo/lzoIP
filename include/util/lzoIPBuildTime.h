#ifndef D_lzoIPBuildTime_H
#define D_lzoIPBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  lzoIPBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class lzoIPBuildTime
  {
  public:
    explicit lzoIPBuildTime();
    virtual ~lzoIPBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    lzoIPBuildTime(const lzoIPBuildTime&);
    lzoIPBuildTime& operator=(const lzoIPBuildTime&);

  };

#endif  // D_lzoIPBuildTime_H
